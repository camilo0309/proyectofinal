import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login:any = {};
  register:any = {};
  eventImage:Event = null;

  constructor(public dialogRef: MatDialogRef<LoginComponent>, private authentication:AuthenticationService, private router:Router) { }
  ngOnInit() { }

  selectImage(event){
      this.eventImage = event;
  }

  public signIn(){
      this.authentication.signIn(this.login, this.dialogRef);
  }

  public registerUser(){
      this.register.rol = 2;
      this.authentication.register(this.register, this.dialogRef);
  }

}
