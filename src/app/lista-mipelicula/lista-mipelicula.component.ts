import { FirebaseService } from './../services/firebase.service';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-mipelicula',
  templateUrl: './lista-mipelicula.component.html',
  styleUrls: ['./lista-mipelicula.component.css']
})
export class ListaMipeliculaComponent implements OnInit {
  dataUser = null;
  postsUser = null;

  constructor(private firebase: FirebaseService, private authentication: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
    this.dataUser = this.authentication.getDataUser();
    this.postsUser = this.firebase.getPostsUser(this.dataUser.id);
  }

  viewFormUpdatePost(id) {
    this.router.navigate(['/createPelicula/' + id]);
  }

  deleteImage(id) {
    this.firebase.deletePost(id);
  }

  viewDetailPost(id) {
    this.router.navigate(['/detallePelicula/' + id]);
  }



}
