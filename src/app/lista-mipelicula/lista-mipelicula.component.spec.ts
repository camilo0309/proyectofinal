import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaMipeliculaComponent } from './lista-mipelicula.component';

describe('ListaMipeliculaComponent', () => {
  let component: ListaMipeliculaComponent;
  let fixture: ComponentFixture<ListaMipeliculaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaMipeliculaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaMipeliculaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
