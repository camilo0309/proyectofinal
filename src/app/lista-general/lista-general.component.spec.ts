import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaGeneralComponent } from './lista-general.component';

describe('ListaGeneralComponent', () => {
  let component: ListaGeneralComponent;
  let fixture: ComponentFixture<ListaGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
