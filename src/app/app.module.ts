import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material';
import { FormsModule }   from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database'
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';

import { AuthenticationService } from './services/authentication.service';
import { FirebaseService } from './services/firebase.service';


import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PeliculaComponent } from './pelicula/pelicula.component';
import { DetallePeliculaComponent } from './detalle-pelicula/detalle-pelicula.component';
import { InicioComponent } from './inicio/inicio.component';
import { ListaGeneralComponent } from './lista-general/lista-general.component';
import { ListaMipeliculaComponent } from './lista-mipelicula/lista-mipelicula.component';

const appRoutes:Routes = [
	{path: '', component: InicioComponent},
	{path: 'peliculasGeneral', component: ListaGeneralComponent},
	{path: 'misPeliculas', component: ListaMipeliculaComponent},
	{path: 'createPelicula/:status', component: PeliculaComponent},
	{path: 'detallePelicula/:id', component: DetallePeliculaComponent},
];


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PeliculaComponent,
    DetallePeliculaComponent,
    InicioComponent,
    ListaGeneralComponent,
    ListaMipeliculaComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireStorageModule
  ],
  providers: [AuthenticationService, FirebaseService],
  bootstrap: [AppComponent],
  entryComponents: [
    LoginComponent
  ]
})
export class AppModule { }
